from __future__ import print_function
from qtpy import QtWidgets, QtCore, QtGui
from qtpy.QtGui import QTextCursor, QTextCharFormat
from kabaret.app import resources
import json
import os
import time
import gazu
from libreflow.baseflow.kitsu import KitsuAPIWrapper as kitsu
from kabaret import flow
import sys

try:
    from kabaret.app.ui.gui.widgets.widget_view import DockedView
except ImportError:
    raise RuntimeError('Your kabaret version is too old, for the Custom View. Please update !')
import datetime


class OutPutTextEdit(QtWidgets.QPlainTextEdit):

    INTRO_TEXT = (
        'This is meant to be used as an output window\n'
        'Would be cool to use this one to print text instead of the prompting one.'
    )
    cursor = None

    def __init__(self, parent, session):
        
        super(OutPutTextEdit, self).__init__(parent, session)
        self.session = session
        

        self.setReadOnly(True)
        doc = self.document()
        self.cursor = QTextCursor(doc)
        font = doc.defaultFont()
        font.setFamily('Monospace')
        doc.setDefaultFont(font)

        self.setAcceptDrops(True)
        self.clear()

    def append_text(self, text):
        
        # Append text to the existing content
        doc = self.document()
        cursor = QTextCursor(doc)
        cursor.movePosition(cursor.End)
        current_time = datetime.datetime.now().time()
        formatted_time = current_time.strftime('%H:%M:%S')
        cursor.insertText(str(formatted_time) + " |  " + text + "\n")

    def clear(self):
        super(OutPutTextEdit, self).clear()
        try:
            self.setPlaceholderText(self.INTRO_TEXT)
        except AttributeError:
            # Not available before Qt5.3
            self.setPlainText(self.INTRO_TEXT)

    def start_capture_std(self):
        self._std_capture.start_capture()
        return self._std_capture

    def stop_capture_std(self):
        self._std_capture.stop_capture()

class LogView(DockedView):

    @classmethod
    def view_type_name(cls):
        return 'Log View'

    def __init__(self, session, *args, **kwargs):
        super(LogView, self).__init__(session,*args, **kwargs)
        self.session = session
    def _build(self, top_parent, top_layout, main_parent, header_parent, header_layout):
        # self.add_header_tool('*', '*', 'Duplicate View', self.duplicate_view)

        self._log_view = OutPutTextEdit(main_parent, self.session)

        lo = QtWidgets.QVBoxLayout()
        lo.setContentsMargins(0, 0, 0, 0)
        lo.setSpacing(0)
        lo.addWidget(self._log_view)

        main_parent.setLayout(lo)
        self.view_menu.setTitle('Log')

        self.view_menu.addAction('clear', self.clear)



    # def blablabla(self):
    #     self._log_view.append_text("blablabla")
    #     wm = self.session
    #     log_view = wm.find_view("Log View")
    #     log_view._log_view.append_text("blublublua")
    #     # print(help(wm))
 
    def clear(self):
        self._log_view.clear()

    def on_show(self):
        # self._log_view.clear()
        pass
        
    def receive_event(self, event, data):
        pass

