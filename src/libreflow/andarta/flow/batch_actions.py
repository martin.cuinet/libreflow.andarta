import os
import tempfile
import json
import gazu
import getpass
import pathlib
import fnmatch
import re
import textwrap
import time

from kabaret.app import resources

from kabaret import flow
from libreflow.baseflow.kitsu import KitsuAPIWrapper as kitsu
from libreflow.baseflow.file import File, OpenWithAction, GenericRunAction

class Episode_Value(flow.values.ChoiceValue):

    _choices = ['EP101', 'EP102', 'EP103', 'EP104', 'EP105', 'EP106', 'EP107', 'EP108', 'EP999']

    def choices(self):
        return self._choices

    def revert_to_default(self):
        choice = self.choices()[0] if self.choices() else None
        self.set(choice)

    def _fill_ui(self, ui):
        super(Episode_Value, self)._fill_ui(ui)


class Sequence_Value(flow.values.ChoiceValue):

    _choices = ['SQ001', 'SQ002', 'SQ003', 'SQ004', 'SQ005', 'SQ006', 'SQ007', 'SQ008', 'SQ009', 'SQ010', 'SQ011', 'SQ012', 'SQ013', 'SQ014', 'SQ015', 'SQ016', 'SQ017', 'SQ018', 'SQ019', 'SQ020', 'SQ021', 'SQ022', 'SQ023', 'SQ024', 'SQ025', 'SQ026', 'SQ027', 'SQ028', 'SQ029', 'SQ030', 'SQ031', 'SQ032', 'SQ033', 'SQ034', 'SQ035', 'SQ036', 'SQ037', 'SQ038', 'SQ039', 'SQ040', 'SQ041', 'SQ042', 'SQ043', 'SQ044', 'SQ045']
    

    def choices(self):
        return self._choices

    def revert_to_default(self):
        choice = self.choices()[0] if self.choices() else None
        self.set(choice)

    def _fill_ui(self, ui):
        super(Sequence_Value, self)._fill_ui(ui)



class Shots_choices(flow.values.MultiChoiceValue):

    HIDDEN = False
    _choices = []
    STRICT_CHOICES = True
    ep = flow.Param([], Episode_Value).watched()
    sq = flow.Param([], Sequence_Value).watched()

    def __init__(self, parent, name):
        self._choices = self.root().project().get_entity_manager().shots.mapped_names()
        

    def choices(self):
        return sorted(self._choices)

    def check_existing_file(self, shot_oid):
        # print(shot_oid)
        layout_bg_oid = shot_oid + "/tasks/LAYOUT_BG"
        layout_bg_task = self.root().get_object(layout_bg_oid)
        # print(layout_bg_oid)
        # print(layout_bg_task)
        files = layout_bg_task.get_files(file_type="Works")
        for file in files:
            if file.name() == "LAY_BG_blend":
                # print(layout_bg_oid + " ---- Built")
                return True

        return False

    def filter_shots(self, choices, ep, sq, filter_built):
        filtered_shots = []
        shot_is_built = False
        to_push = ""
        for shot in choices:
            parts = shot.split("/")
            _ep = parts[3]  
            _sq = parts[5]  
            _sh = parts[7]
            shot_display = _ep+"_"+_sq+"_"+_sh  
            # check if the shot matches the filter
            if ep in shot and sq in shot: 
                to_push = shot_display
                # check if the shot already has a default file if filter_built is true
                shot_is_built = self.check_existing_file(shot)
                if filter_built:
                    
                    if shot_is_built:
                        to_push = (shot_display + " - BUILT")
                        # print(shot_display + " - BUILT")
                        filtered_shots.append(to_push)
                    # print(shot_is_built)
                    else:
                        filtered_shots.append(to_push)
                else:
                    if shot_is_built:
                        continue
                    # print(shot_is_built)
                    else:
                        filtered_shots.append(to_push)

        # print(filtered_shots)
        self._choices = filtered_shots
        self.choices()
        self.touch()

    def _fill_ui(self, ui):
        super(Shots_choices, self)._fill_ui(ui)
        ui['hidden'] = self.HIDDEN

class ToggleBuilt(flow.values.BoolValue):

    DEFAULT_EDITOR = 'bool'

    
    def validate(self, value):
        try:
            return bool(value)
        except TypeError:
            raise TypeError(
                'Invalid value type %r for %s (must be a bool)' % (
                    value, self.oid(),
                )
            )

    def set(self, value):
        value = self.validate(value)
        super(ToggleBuilt, self).set(value)


class Build_BG(GenericRunAction):

    ep = flow.Param([], Episode_Value).watched()
    sq = flow.Param([], Sequence_Value).watched()
    bg_scenes = flow.Param([], Shots_choices)
    Show_Built_Shots = flow.Param([], ToggleBuilt).watched()

    wc_path = ""
    _script_path = ""
    _ep_name = ""
    _seq_name = ""
    _sh_name = ""
    _refs = []
    _additional_shots = []
    _nb_frame = 0
    _frame_ranges = []


    def needs_dialog(self):
        self.ep.revert_to_default()
        self.sq.revert_to_default()
        self.bg_scenes.filter_shots(self.get_all_shots(), self.ep.get(), self.sq.get(), self.Show_Built_Shots.get())
        self.message.set(f'<h2>Build scene BG</h2>')
        return True

    def allow_context(self, context):
        return context and context.endswith(".details")

    def get_all_shots(self):
        return self.root().project().get_entity_manager().shots.mapped_names()

    def child_value_changed(self, child_value):
        # print(self.Show_Built_Shots.get())
        # self.bg_scenes.choices()
        self.bg_scenes.filter_shots(self.get_all_shots(), self.ep.get(), self.sq.get(), self.Show_Built_Shots.get())

    def get_buttons(self):
        return ['Cancel', 'Build BG Scenes']

    def runner_name_and_tags(self):
        return 'Blender', []
    
    def extra_argv(self):
        return [ self.wc_path,'--background', '--python', self._script_path]

    def get_shot_info(self, task):
        # get episode sequence and shot name 
        oids = [oid for _, oid in self.root().session().cmds.Flow.split_oid(task.oid())]
        print("Oids : ")
        print(oids)
        episode_oid, seq_oid, shot_oid = oids[-4:-1]
        
        self._ep_name = self.root().get_object(episode_oid).name()
        self._seq_name = self.root().get_object(seq_oid).name()
        self._sh_name = self.root().get_object(shot_oid).name()
        print("-------------------------------")
        print(self._ep_name)
        print(self._seq_name)
        print(self._sh_name)
        print("-------------------------------")


        if kitsu.current_user_logged_in(self):
            # print("User logged in...")
            project = gazu.project.get_project_by_name("EWILAN")
            episode = gazu.shot.get_episode_by_name(project, self._ep_name)
            sequence = gazu.shot.get_sequence_by_name(project,self._seq_name,episode)
            shot = gazu.shot.get_shot_by_name(sequence, self._sh_name)
            task_type = gazu.task.get_task_type_by_name("LAYOUT_BG")
            task = gazu.task.get_task_by_name(shot,task_type)
            self._nb_frame = shot["nb_frames"]
            print(shot)
            shot_data = shot["data"]
            # Check if the shot has any "use_bg_from" data
            if "use_bg_from" in shot_data:
                print("shot has this data")
                # Check if the data is not none
                data = shot_data["use_bg_from"]
                if len(data)>1:
                    use_bg = shot["data"]["use_bg_from"]
                    return use_bg
                else:
                    self.get_refs(task, project)
                    return True
            else:
                self.get_refs(task, project)
                return True

    def get_refs(self, task, project):

        self._refs = []
        self._additional_shots = []
        self._frame_ranges = []

        pattern = re.compile(r'^EP\d{3}_SQ\d{3}_SH\d{4}$')
        comments = gazu.task.all_comments_for_task(task)
        if comments:
            for item in comments[-1]["checklist"]:
                if pattern.match(item["text"]):
                    self._additional_shots.append(item["text"])
                else:
                    self._refs.append(item["text"])

            # Get the frame ranges of the additional shots
            for shot in self._additional_shots:
                ep, seq, sh = shot.split("_")
                episode = gazu.shot.get_episode_by_name(project, ep)
                sequence = gazu.shot.get_sequence_by_name(project,seq,episode)
                additional_shot = gazu.shot.get_shot_by_name(sequence, sh)    
                self._frame_ranges.append({"shot": shot, "nb_frame": additional_shot["nb_frames"]})
            
            self._frame_ranges.append({"shot": self._ep_name+"_"+self._seq_name+"_"+self._sh_name, "nb_frame": self._nb_frame})
            print(self._frame_ranges)
            return True
        else:
            self._frame_ranges.append({"shot": self._ep_name+"_"+self._seq_name+"_"+self._sh_name, "nb_frame": self._nb_frame})
            print(self._frame_ranges)
            print("No comment found")
            return False

    def write_script(self, choice):

        print("--------------------------------------------")

        print("Write script function")
        print("REFS : ", self._refs)
        print("ADDITIONAL_SHOTS : ", self._additional_shots)

        print("--------------------------------------------")
        build_scene_script_content = textwrap.dedent(f'''\
        import bpy
        import os

        # Get the active camera object
        active_camera = bpy.context.scene.camera

        # Check if there is an active camera
        if active_camera:
            # Rename the camera to a new name (change "NewCameraName" to your desired name)
            new_camera_name = "CAM_{self._seq_name}_{self._sh_name}"
            active_camera.name = new_camera_name

        # Set the frame range
        start_frame = 1
        end_frame = max(entry['nb_frame'] for entry in {self._frame_ranges})

        # Set the frame start and end for the current scene
        bpy.context.scene.frame_start = start_frame
        bpy.context.scene.frame_end = end_frame

        # Set the path to the movie clip
        movie_clip_path = "K:/02_PRODUCTIONS/01_EWILAN/AA_EWI_PROD/{self._ep_name}/ANIMATIC/TO_PIPE/MOV/{self._seq_name}_{self._sh_name}.mov"

        # Set the camera as the active object
        bpy.context.view_layer.objects.active = bpy.context.scene.camera

        # Add a background image to the camera
        background_image = active_camera.data.background_images.new()

        background_image.clip = bpy.data.movieclips.load(movie_clip_path)

        # Set the background image options
        background_image.frame_method = 'FIT'
        background_image.source = 'MOVIE_CLIP'

        
        # Create new Camera(s) if needed
        print("Additional shots : ", {self._additional_shots})
        for shot in {self._additional_shots}:
            # Create a new camera
            sq_index = shot.find("SQ")
            if sq_index != -1:
                result = shot[sq_index:]
            
            duplicated_camera = active_camera.copy()
            duplicated_camera.data = active_camera.data.copy()
            duplicated_camera.name = "CAM_"+result

            # Put the new camera in the collection "COL-RDR_CAM_BUILD"
            Cam_collection = bpy.data.collections["COL-RDR_CAM_BUILD"]
            Cam_collection.objects.link(duplicated_camera)

            # Modifiy the background clip for the new camera
            movie_clip_path = "K:/02_PRODUCTIONS/01_EWILAN/AA_EWI_PROD/{self._ep_name}/ANIMATIC/TO_PIPE/MOV/"+result+".mov"
            background_image = duplicated_camera.data.background_images[0]
            background_image.clip = bpy.data.movieclips.load(movie_clip_path)

                
        # Import ref image
        for ref in {self._refs}:
            ref_path = "K:/02_PRODUCTIONS/01_EWILAN/AA_EWI_PROD/{self._ep_name}/BG/" + ref + "/04_REF_DESIGN/TO_PIPE"
            print("ref_path : ", ref_path)
            image_files = [f for f in os.listdir(ref_path) if f.lower().endswith(('.png', '.jpg'))]
            for image_file in image_files:
                # Set the image path for the reference image
                image_path = os.path.join(ref_path, image_file)

                # Load image as a reference image (Image as Plane)
                bpy.ops.object.load_reference_image(filepath=image_path)

                # Get the reference image object
                reference_image = bpy.context.object
                reference_image.name = image_file

                # scale the empty
                reference_image.empty_display_size = 20

                # Rotate the reference image by 90 degrees on the X-axis
                reference_image.rotation_euler.x += 1.5708  # 1.5708 radians is equivalent to 90 degrees

                # unlink the object collection before moving it to the right one.
                bpy.context.collection.objects.unlink(reference_image)        

                Ref_collection = bpy.data.collections["COL_REF"]
                Ref_collection.objects.link(reference_image)
        

        # set the timeline markers 
        for entry in {self._frame_ranges}:
            shot_name = entry['shot']
            frame_number = entry['nb_frame']

            # Create a timeline marker
            bpy.context.scene.timeline_markers.new(name=shot_name, frame=frame_number)
                
        bpy.ops.wm.save_mainfile()                                   

        ''')

        temp_folder = tempfile.gettempdir()
        
        build_scene_script_path = os.path.join(temp_folder, 'build_scene_script.py')
        with open(build_scene_script_path, 'w') as file:
            file.write(build_scene_script_content)

        self._script_path = build_scene_script_path
        print(self._script_path)
        return build_scene_script_path
    

    def get_version(self, button):
        return None


    def run(self, button):
        if button == "Cancel":
            print("Cancelled")
            return
        
        print("Build BG scenes")
        for choice in sorted(self.bg_scenes.get()):
            time.sleep(3)
            parts = choice.split("_")
            shot_BG_task_oid = f'/EWILAN/films/{parts[0]}/sequences/{parts[1]}/shots/{parts[2]}/tasks/LAYOUT_BG'
            print(shot_BG_task_oid)
            layout_bg_task = self.root().get_object(shot_BG_task_oid)
            print("Creating default files for : ", layout_bg_task)
            layout_bg_task.create_dft_files.run("OK")
            files = layout_bg_task.get_files(file_type = "Works")
            for file in files:
                # print(file)
                
                file.create_working_copy(from_revision = file.get_head_revision().name())
                file.publish(revision_name=None, source_path=None, comment=('Scene Built'), keep_editing=False, ready_for_sync=True, path_format=None)
                head_revision = file.get_head_revision().get_path()
                self.wc_path = head_revision
                print(self.wc_path)
                shot_info = self.get_shot_info(layout_bg_task)
                if shot_info == True:
                    self.write_script(choice)
                    super(Build_BG, self).run(button)
                    ## GenericRunAction.run(self, button)
                    
                elif type(shot_info) != bool:
                    print("cannot build ", self.wc_path, " Bg used in ", shot_info)
                    continue

