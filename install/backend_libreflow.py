import sys
import os
import requests
import json
import argparse
import re
import subprocess
import pkg_resources

remote_path = r"https://gitlab.com/api/v4/projects/47996332/repository/files/config.json/raw"


def check_poetry_install():
    try:
        pkg_resources.get_distribution('poetry')
    except pkg_resources.DistributionNotFound:
        subprocess.call(['pip', 'install', 'poetry'])
    

def set_new_remote(path):
    global remote_path
    if os.path.isfile(path):
        path = path.replace('\\', '\\\\')

    remote_path = path
    
    with open(os.path.realpath(__file__), 'r') as f:
        lines = f.readlines()
        lines[9] = 'remote_path = r"{}"\n'.format(path)
    
    with open(os.path.realpath(__file__), 'w') as f:
        f.writelines(lines)


def fetch_config(local_path):
    local_path += '\config.json'

    global remote_path
    # GET
    if remote_path.startswith('http'):
        request = requests.get(remote_path)
        remote_config = json.loads(request.text)
    elif remote_path.endswith('.json'):
        with open(remote_path, "r") as f:
            remote_config = json.loads(f.read())

    status = False
    if os.path.exists(local_path):
        with open(local_path, "r") as f:
            local_config = json.loads(f.read())

        # COMPARE
        if local_config != remote_config:
            with open(local_path, 'w') as f:
                # Write new content to the file
                json.dump(remote_config, f)

            local_config = remote_config
            status = True

    else:
        with open(local_path, 'w') as f:
            json.dump(remote_config, f)

        local_config = remote_config
        status = True

    return status, local_config


def update_env(data, local_path):
    data['extensions'] = sorted(data['extensions'], key=lambda x: x['name'])
    main_dependencies_list = ['kabaret', 'libreflow', 'libreflow-andarta']
    toml_path = f"{local_path}/pyproject.toml"

    # Prepare extensions (dependencies) list
    requiered_extensions = {}

    for extension in data['extensions']:
        if not extension['version']['is_enabled']:
            continue

        version = extension['version']

        if version['service'] == "pypi":
            # Using a specific version
            if '==' in version["pypi"]:
                name, number = version["pypi"].split("==")
                name = re.sub('[._+]', '-', name)
                requiered_extensions[name] = number
            # Using any version
            else:
                name = re.sub('[._+]', '-', version["pypi"])
                requiered_extensions[name] = "*"

        elif version['service'] == "gitlab":
            name = re.sub('[._+]', '-', extension['name'])
            requiered_extensions[name] = {"git": version['url']}

            if version['repo_ref_type'] == "branch":
                requiered_extensions[name]["branch"] = version['repo_ref']
            elif version['repo_ref_type'] == "commit":
                requiered_extensions[name]["rev"] = version['repo_ref']

        elif version['service'] == "gitlab-url":
            name = re.sub('[._+]', '-', extension['name'])
            requiered_extensions[name] = {"git": version['url']}

    # Extract main dependencies
    main_dependencies = {
        k: v for k, v in requiered_extensions.items() if k in main_dependencies_list}

    # Extract extensions
    extensions_dependencies = {
        k: v for k, v in requiered_extensions.items() if k not in main_dependencies_list}

    # Format for toml config
    main_dependencies_string = """"""
    for k, v in main_dependencies.items():
        main_dependencies_string += f"{k} = "
        if type(v) is not dict:
            main_dependencies_string += f'"{v}"\n'
        else:
            for i, (key, value) in enumerate(main_dependencies[k].items()):
                main_dependencies_string += ', ' if i > 0 else '{ '
                main_dependencies_string += '{key} = "{value}"'.format(
                    key=key, value=value)

                if i == len(main_dependencies[k].items())-1:
                    main_dependencies_string += ' }'

            main_dependencies_string += "\n"

    extensions_dependencies_string = """"""
    if len(extensions_dependencies) > 0:
        for k, v in extensions_dependencies.items():
            extensions_dependencies_string += f"{k} = "
            if type(v) is not dict:
                extensions_dependencies_string += f'"{v}"\n'
            else:
                for i, (key, value) in enumerate(extensions_dependencies[k].items()):
                    extensions_dependencies_string += ', ' if i > 0 else '{{ '
                    extensions_dependencies_string += '{key} = "{value}"'.format(
                        key=key, value=value)

                    if i == len(extensions_dependencies[k].items())-1:
                        extensions_dependencies_string += ' }'

                extensions_dependencies_string += "\n"

    # Create toml content
    toml_content = """\
[tool.poetry]
name = "andarta-libreflow-poetry"
version = "2.0.0"
description = "This project defines a flow dedicated to Andarta Pictures productions."
authors = [
    "Baptiste Delos <baptiste@les-fees-speciales.coop>",
    "Flavio Perez <flavio@lfs.coop>",
    "Valentin Braem <valentinbraem1@gmail.com>"
    ]
license = "LGPLv3+"

[tool.poetry.dependencies]
python = "{python_version}"
{main_list}
[tool.poetry.group.extensions.dependencies]
{extensions_list}

[build-system]
requires = ["poetry-core"]
build-backend = "poetry.core.masonry.api" """.format(
        python_version=re.match(r"([^\s]+)", sys.version).group(1),
        main_list=main_dependencies_string,
        extensions_list=extensions_dependencies_string
    )

    with open(toml_path, "w") as f:
        f.write(toml_content)
        f.close()


def generate_start_bat(data):
    command = [
        "poetry",
        "run",
        "python",
        "-m",
        "libreflow.andarta.gui",
        "--host",
        "%REDIS_HOST%",
        "--port",
        "%REDIS_PORT%",
        "--db",
        "%REDIS_DB%",
        "--cluster",
        "%LF_CLUSTER_NAME%",
        "--session",
        "libreflow",
        "--site",
        "%LF_SITE_NAME%",
        "--password",
        "%REDIS_PASSWORD%",
        "--search-index-uri",
        "%MONGO_URI%"
    ]

    bat_content = """\
@echo off

set CURRENT_DIR=%~dp0

:: Set console to tech dir
cd /d %TECH_DIR%

:: Update check
python backend_libreflow.py --config %CURRENT_DIR% > Output
set /p UPDATE_STATUS=<Output
del Output

cd /d %CURRENT_DIR%
if %UPDATE_STATUS% == True (
    poetry update
)

:: Start command
{command}
pause""".format(
        command=' '.join(command)
    )

    bat_path = f"{bat_folder}/libreflow.bat"

    with open(bat_path, 'w+') as f:
        f.write(bat_content)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', '-c', type=str, help="Specify local copy of config")
    parser.add_argument('--remote', '-r', type=str, help="Specify a new remote config path")
    parser.add_argument('--install', '-i', action='store_true', help="Start installation process")
    args = parser.parse_args()
    
    if args.remote is not None:
        set_new_remote(args.remote)
    elif args.install:
        bat_folder = input("Enter the path for starting bat: ")
        # remote_input = input("Enter json config remote path (local or from a Gitlab REST URL): ")
        # set_new_remote(remote_input)
        
        check_poetry_install()
        
        update, data = fetch_config(bat_folder)
        update_env(data, bat_folder) if update else None
        
        os.chdir(bat_folder)
        subprocess.call(['poetry', 'install'])
        generate_start_bat(data)
    elif args.config is None:
        print('ERROR: You need to specify the local path of config with --config, to be able to compare with the remote')
    else:
        update, data = fetch_config(args.config)
        update_env(data, args.config) if update else None
        print(update)
